from os import urandom, listdir
import os
import shutil
import json
import PyPDF3
import tarfile
import subprocess

from articles.article_download import get_new_id
from clean_text import clean_text
from index_to_elasticsearch import UploadElasticSearch

my_directory = "/Users/thedude/desktop/arxiv/src"
new_counter = 0


def extract_text_pdfs(directory, counter):
    processed_json = []
    for filename in listdir(directory):
        if filename.endswith(".pdf"):
            path = os.path.join(directory, filename)

            pdfFileObj = open(path, 'rb')
            pdfReader = PyPDF3.PdfFileReader(pdfFileObj)
            num_pages = pdfReader.numPages
            count = 0
            text = ""
            while count < num_pages:
                pageObj = pdfReader.getPage(count)
                count += 1
                text += "\n" + pageObj.extractText()
            # f = open(path, 'r')
            # text = f.read()
            # text = text.encode("utf-8", errors="replace")
            # text = text.decode("utf-8")
            # clean_text = clean_article(text)
            # clean_title = clean_title(filename)
            new_seed = urandom(12)
            new_json = {"type": "add", "id": str(get_new_id(counter, new_seed)),
                        "fields": {
                            "raw_text": text,
                            # "title": clean_title,
                            "raw_title": filename,
                            # "text": clean_text,
                            "url": "www.example.com",
                            "publish_date": "blah",
                        }
                        }
            processed_json.append(new_json)
            counter += 1
        else:
            continue

    f = open('test.json', 'w', encoding="utf-8")
    data = json.dumps(processed_json)
    f.write(data)
    f.close()


def extract_tar_arxiv(directory):
    """
    Loops through directory of tar files, extracts gz, extracts latex from gz, then processes latex to remove latex
    tags. Raw text is saved in new directory for now.

    :param directory:
    :param counter:
    :return:
    """

    es = UploadElasticSearch()
    es.get_config()

    for filename in listdir(directory):
        # Look for tar file
        if filename.endswith(".tar"):
            path = os.path.join(directory, filename)

            tar = tarfile.open(name=path, mode='r')
            tar.extractall(path=my_directory)

            for name in listdir(directory):
                dir_path = os.path.join(directory, name)
                if os.path.isdir(dir_path):
                    for gz_file in os.listdir(dir_path):
                        gz_path = os.path.join(dir_path, gz_file)
                        # print(gz_path)
                        if gz_path.endswith(".pdf"):
                            continue
                        if not gz_path.endswith(".gz"):
                            continue
                        new_gz_dir = gz_path[:-3]
                        try:
                            os.mkdir(new_gz_dir)
                        except FileExistsError:
                            pass
                        cmd = ['gunzip', '-c', gz_path, '|', 'tar', 'xopf', '-', '-C', new_gz_dir]
                        subprocess.call(' '.join(cmd), shell=True)

                        for temp in os.listdir(new_gz_dir):
                            if temp.endswith(".tex"):
                                tex_path = os.path.join(new_gz_dir, temp)
                                with open(tex_path, 'r') as f:
                                    try:
                                        data = f.read()
                                    except UnicodeDecodeError:
                                        continue

                                    # TODO GET into/author/title etc
                                    intro = "test"
                                    author = "testing author"
                                    title = "I am a title"
                                    abstract = "Abstract!"
                                    year = "2019"
                                    url = "example.com"

                                detex = ['detex', '-r', '-s', tex_path, "|", 'tee',
                                         '/users/thedude/desktop/rawtext/'+temp+".txt"]
                                subprocess.call(' '.join(detex), shell=True, stdout=subprocess.DEVNULL)

                                with open("/users/thedude/desktop/rawtext/"+temp+".txt", 'r') as x:
                                    text = clean_text(x.read())

                                es.upload_academic(text, "title", "2019", "author", url)


def extract_tar_file(filename):
    """
    Loops through directory of tar files, extracts gz, extracts latex from gz, then processes latex to remove latex
    tags. Raw text is saved in new directory for now.

    :param directory:
    :param counter:
    :return:
    """
    # TODO Be careful with these for loops. We MUST avoid duplicates!
    es = UploadElasticSearch()
    root_dir = "data/"
    src_dir = "data/src"
    destination = "data/gz_dir"
    extracted_text_file = "data/clean_text/temp.txt"

    if filename.endswith(".tar"):

        tar = tarfile.open(name=root_dir + filename, mode='r')
        tar.extractall(path=destination)

        # delete tar file
        os.remove(root_dir + filename)

        for name in listdir(destination):
            dir_path = os.path.join(destination, name)
            if os.path.isdir(dir_path):
                for gz_file in os.listdir(dir_path):
                    gz_path = os.path.join(dir_path, gz_file)
                    if gz_path.endswith(".pdf"):
                        continue
                    if not gz_path.endswith(".gz"):
                        continue
                    new_gz_dir = gz_path[:-3]
                    try:
                        os.mkdir(new_gz_dir)
                    except FileExistsError:
                        pass
                    cmd = ['gunzip', '-c', gz_path, '|', 'tar', 'xopf', '-', '-C', new_gz_dir]
                    subprocess.call(' '.join(cmd), shell=True)

                    for temp in os.listdir(new_gz_dir):
                        if temp.endswith(".tex"):
                            tex_path = os.path.join(new_gz_dir, temp)
                            # TODO Check if directory. Also, maybe needs to just delete because it doesn't do anything
                            with open(tex_path, 'r') as f:
                                try:
                                    data = f.read()
                                except UnicodeDecodeError:
                                    continue

                                # TODO GET intro/author/title etc
                                intro = "test"
                                author = "testing author"
                                title = "I am a title"
                                abstract = "Abstract!"
                                year = "2019"
                                url = "example.com"

                            # TODO use microsoft to extract from pdfs instead of text
                            if tex_path.endswith('.tex'):
                                detex = ['detex', '-s', tex_path, "|", 'tee',
                                         'data/clean_text/temp' + ".txt"]
                                subprocess.call(' '.join(detex), shell=True, stdout=subprocess.DEVNULL)

                                # TODO Check if text file is a directory
                                with open(extracted_text_file, 'r') as x:
                                    try:
                                        text = clean_text(x.read())
                                    except:
                                        continue

                                es.upload_academic(text, "title", "2019", "author", url)
                        else:
                            break
                    # remove gz extracted directory
                    shutil.rmtree(new_gz_dir)
                # Remove tar extracted directory
                shutil.rmtree(dir_path)

