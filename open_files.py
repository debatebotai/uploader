# -*- coding: utf-8 -*-
"""

"""
import os
from docx import Document
import PyPDF2 as pypdf
from nltk.tokenize import sent_tokenize
import re

def open_word(_filename, _extention):
    document = Document(_filename + _extention)
    all_text = ''
    for paragraph in document.paragraphs:
        if paragraph.text[:-1] != '.':
            add_full_stop = '.'
        else:
            add_full_stop = ''
        all_text += ' ' + paragraph.text + add_full_stop
    return _filename, all_text


def open_pdf(_filename, _extention):
    all_text = ''
    pdf_open = open(_filename + _extention,'rb')
    pdf = pypdf.PdfFileReader(pdf_open)
    info = pdf.getDocumentInfo()
    title = info.title
    num_pages = pdf.numPages
    for i in range(num_pages):
        page = pdf.getPage(i)
        all_text += page.extractText() + ' '
    return title, all_text


def extract_from_file(_document):
    filename, extension = os.path.splitext(_document)
    if extension == '.doc' or extension == '.docx':
        title, all_text = open_word(filename, extension)
    elif extension == '.pdf':
        title, all_text = open_pdf(filename, extension)
    return title, all_text
