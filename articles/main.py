from articles.article_download import get_articles, get_urls, get_new_id

import pandas as pd
import logging
from os import urandom
import json

logger = logging.getLogger(__name__)


def cloudsearch_articles():
    # Get all article urls from rss feeds
    rss_urls = ['http://rss.cnn.com/rss/cnn_topstories.rss',
                # 'https://www.vox.com/rss/index.xml',
                # 'http://feeds.foxnews.com/foxnews/latest',
                # 'http://feeds.reuters.com/reuters/MostRead',
                # 'http://feeds.reuters.com/Reuters/PoliticsNews',
                # 'http://rss.nytimes.com/services/xml/rss/nyt/World.xml',
                # 'http://feeds.bbci.co.uk/news/world/rss.xml',
                # 'http://feeds.washingtonpost.com/rss/world',
                ]

    home_pages = ['bloomberg.com', ]

    ''' Grab articles from rss '''
    # url_list = get_urls(rss_urls, home_pages)
    url_list = ["https://www.aginginplace.org/cellphone-guide-for-seniors/", "http://www.caregiverresourcecenter.com/advanced_elder_care_planning.htm", "https://assistedlivingtoday.com/blog/the-importance-of-phones-for-the-elderly/"]
    articles = get_articles(url_list)
    logger.info('articles collected: %d', len(articles))

    ''' save articles to a csv '''
    data = pd.DataFrame(articles)
    data.to_csv('articles.csv')
    logger.info('Articles exported to csv')


    ''' Load articles from csv'''
    data = pd.read_csv('articles.csv')
    # logger.info(len(data.values))
    data.fillna(0, inplace=True)
    data = data.values
    new_counter = 0

    ''' Convert to json format for CloudSearch on AWS. Needs a unique id'''
    logger.info('Converting to JSON')
    processed_json = []
    for art in data:
        new_seed = urandom(12)
        new_json = {"type": "add", "id": str(get_new_id(new_counter, new_seed)),
                    "fields": {
                    "raw_text": art[2],
                    "title": art[5],
                    "raw_title": art[3],
                    "text": art[4],
                    "url": art[6],
                    "publish_date": art[1],
                            }
                    }
        processed_json.append(new_json)
        new_counter += 1
        logger.info('processed %d', new_counter)

    f = open('upload.json', 'w')
    json.dump(processed_json, f)
    f.close()
    logger.info('Done')


cloudsearch_articles()
