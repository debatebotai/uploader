import feedparser
from newspaper import Article

import boto3
import pandas as pd
from nltk.tokenize import word_tokenize, sent_tokenize

import logging
import time
import hashlib
from os import urandom


# logging.basicConfig(filename='logs/article_upload.log', level=logging.INFO,
#                     format='%(asctime)s %(message)s')
logger = logging.getLogger(__name__)


def get_urls(rss_urls, home_pages):
    url_list = []
    for url in rss_urls:
        data = feedparser.parse(url)
        for i in data['entries']:
            url_list.append(i['link'])
    for url in home_pages:
        pass

    return url_list


def get_articles(rss_urls):
    art_list = []
    count = 0
    for url in rss_urls:
        count += 1
        logger.info('Getting article %s', url)
        art_obj = {'title': '', 'text': '', 'url': '', 'publish_date': ''}
        art = Article(url)
        art.download()
        art.parse()
        art_obj['raw_text'] = art.text
        art_obj['text'] = clean_article(art.text)
        art_obj['url'] = art.url
        art_obj['title'] = clean_title(art.title)
        art_obj['raw_title'] = art.title
        art_obj['publish_date'] = art.publish_date
        art_list.append(art_obj)
        if count % 5 == 0:
            time.sleep(1)
        logger.info("article collected %d", count)
        # art_list.append(clean_article(art.text))
    return art_list


# Strip html from rss
def clean_article_rss(article):
    article = article.lower()
    for i in range(len(article)):
        if article[i] == '<':
            idx = i
            break
    clean_art = article[:idx]
    return clean_art


# parse article from newspaper
def clean_article(article):
    article = article.lower()
    # article = article.translate("(){}<>\\")
    article = article.replace('\r', '').replace('\n', '')
    return word_tokenize(article),


def clean_title(title):
    title = title.lower()
    # title = title.translate("(){}<>\\")
    title = title.replace('\r', '').replace('\n', '')
    return word_tokenize(title)


def get_html_images(rss_urls):
    all_images = []
    for url in rss_urls:
        art = Article(url)
        art.download()
        art.parse()
        all_images.extend(art.images)
        print(all_images)
    return all_images


def get_new_id(counter, new_seed):
    h = hashlib.sha1()
    val = (str(counter) + str(new_seed)).encode('utf-8')
    h.update(val)
    return h.hexdigest()



