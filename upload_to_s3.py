# -*- coding: utf-8 -*-
"""

"""

import boto3
import ntpath

s3 = boto3.resource('s3')
bucket_name = 'debatebot-elasticsearch'


def upload_to_s3(directory, bucket_name):
    name = ntpath.basename(directory)
    s3.upload_file(directory, bucket_name, name)


def receive_file_list(list_of_files_to_upload, bucket_name):
    for file in list_of_files_to_upload:
        upload_to_s3(directory, bucket_name)


