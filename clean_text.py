# -*- coding: utf-8 -*-
"""

"""
import re
from nltk.tokenize import sent_tokenize


def clean_text(_all_text):
    all_text = _all_text.replace('\n', '')
    all_sent = sent_tokenize(all_text)
    all_text = ' '.join([i for i in all_sent if len(re.findall(r'[\.?()\-",]', i))  < 5])
    return all_text

