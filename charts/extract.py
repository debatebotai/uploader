from PIL import Image
import pytesseract
import fitz

from cloudsearch.scrapers.download import get_urls, get_html_images, get_new_id

import os
import json
import logging
import requests
import time
from os import urandom


import boto3

from nltk import word_tokenize


from app.settings import AWS_KEY, AWS_REGION, AWS_SECRET

# Simple image to string

logging.basicConfig(filename='../logs/download.log', level=logging.INFO,
                    format='%(asctime)s %(message)s')
logger = logging.getLogger(__name__)


class AmazonS3Session:
    # First scan rss feed for urls
    # Download html and extract image urls
    # Download image
    # create unique id
    # OCR image
    # upload image url, id, and tags
    # delete image if necessary

    def __init__(self, aws_access_key_id=AWS_KEY, aws_secret_access_key=AWS_SECRET, region_name=AWS_REGION,
                      bucket_name="dbot-chart-images"):
        self.sess = boto3.Session(
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key)
        self.s3_client = self.sess.resource('s3')
        self.bucket_name = bucket_name

    def upload_images_from_urls(self, image_url_list, local_image_dir, upload=False):
        all_image_json = []

        for u in image_url_list:
            new_seed = urandom(12)
            image_id = get_new_id(22)
            image_id = image_id + ".png"
            f = open("Data/" + image_id, 'wb')
            f.write(requests.get(u).content)
            f.close()
            time.sleep(2)
            clean_tokens = self.extract_words("Data"+ image_id)
            if len(clean_tokens) == 0:
                continue

            if upload:
                temp_data = {"image": "https://s3.amazonaws.com/dbot-chart-images/" + image_id, "tags": clean_tokens}
                temp = "Data/" + image_id
                self.s3_client.meta.client.upload_file(temp, self.bucket_name, image_id)

            new_json = {"type": "add",
                        "id": image_id,
                        "fields": {
                            "image_url": u,
                            "image_tags": clean_tokens,
                            }
                        }

            all_image_json.append(new_json)
        print(all_image_json)

    def upload_images_pdf(self, directory):
        all_image_json = []
        for filename in os.listdir(directory):
            clean_tokens = self.extract_words("Data/" + filename)

            temp_data = {"image": "https://s3.amazonaws.com/dbot-chart-images/" + filename, "tags": clean_tokens}
            temp = "Data/" + filename
            self.s3_client.meta.client.upload_file(temp, self.bucket_name, filename)


            new_json = {"type": "add",
                        "id": filename,
                        "fields": {
                            "image_link": "https://s3.amazonaws.com/dbot-chart-images/" + filename,
                            "image_tags": clean_tokens,
                        }
                        }

            all_image_json.append(new_json)
            f = open("image_json", 'w')
            f.write(json.dumps(all_image_json))
            f.close()


    @staticmethod
    def extract_words(image_path):
        text = pytesseract.image_to_string(Image.open(image_path))
        text = text.replace("\n", "")
        text = text.replace("'", "")
        tokens = word_tokenize(text)
        clean_tokens = [word for word in tokens if word.isalpha()]
        return clean_tokens


    # TODO Need pdf research or html research feed
    def extract_image_urls_from_rss(self, feeds, home_pages):
        urls = get_urls(feeds, home_pages)
        image_urls = get_html_images(urls)
        print(image_urls)
        return image_urls

    def extract_image_urls_website(self, url):
        image_urls = get_html_images(url)
        return image_urls

    #   TODO How does this work
    def extract_images_pdf(self, pdf_path):
        doc = fitz.open(pdf_path)
        path = "Data/"
        for i in range(len(doc)):
            for img in doc.getPageImageList(i):
                xref = img[0]
                pix = fitz.Pixmap(doc, xref)
                image_id = get_new_id(92)
                if pix.n < 5:  # this is GRAY or RGB
                    pix.writePNG(path + image_id + ".png")
                else:  # CMYK: convert to RGB first
                    pix1 = fitz.Pixmap(fitz.csRGB, pix)
                    pix1.writePNG(path + image_id + ".png")
                    pix1 = None
                pix = None

    def download_pdfs(self, urls):
        for u in urls:
            pdf_id = get_new_id(22)
            pdf_id = "Data/" + pdf_id + ".pdf"
            f = open(pdf_id, 'wb')
            f.write(requests.get(u).content)
            f.close()
            self.extract_images_pdf(pdf_id)
            os.remove(pdf_id)

# TODO Need to add source of image

# Need to
# sess = AmazonS3Session()
# # rss = ["https://www.gov.uk/government/statistics.atom?publication_filter_option=statistics"]
# # rss = ["http://www.afia.org/feedindustrystats"]
# # all_images = sess.extract_image_urls_website(rss)
# # sess.upload_images_cloudsearch(all_images, "Data/", upload=True)
#
# # sess.download_pdfs(["https://www.reri.org/research/files/Ghent.pdf"])
# sess.upload_images_pdf("Data/")
