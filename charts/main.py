from charts.extract import AmazonS3Session



sess = AmazonS3Session()
# rss = ["https://www.gov.uk/government/statistics.atom?publication_filter_option=statistics"]
# rss = ["http://www.afia.org/feedindustrystats"]
# all_images = sess.extract_image_urls_website(rss)
# sess.upload_images_cloudsearch(all_images, "Data/", upload=True)

sess.download_pdfs(["https://www.reri.org/research/files/Ghent.pdf"])
sess.upload_images_pdf("Data/")
