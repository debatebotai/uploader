# -*- coding: utf-8 -*-

from elasticsearch import Elasticsearch, RequestsHttpConnection, helpers
from requests_aws4auth import AWS4Auth
import os
from elasticsearch_dsl import Search

from pdfs.pdf_download import *

os.environ["URL"] = "30ae9d3b1e7f433ba5883987961f530e.us-east-1.aws.found.io"
os.environ["USER"] = "elastic"
os.environ["PASS"] = "OORdNGmDIexOwk981ZKUASMd"

# os.environ["ACCESS"] = "AKIAIXLO7KWULQAFFA5Q"
# os.environ["SECRET"] = "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"
# os.environ["REGION"] = "us-east-1"
# os.environ["TOKEN"] = ""


class Config:

    def __init__(self):
        self.username = ""
        self.password = ""
        self.region = ""
        self.session_token = ""
        self.url = ""

    def get_config(self):
        self.username = os.getenv("USER")
        self.password = os.getenv("PASS")
        # self.region = os.getenv("REGION")
        # self.session_token = os.getenv("TOKEN")
        self.url = os.getenv("URL")


class UploadElasticSearch(Config):

    def __init__(self):
        Config.__init__(self)
        self.es = Elasticsearch
        self.get_config()
        self.activate_es()

    def activate_es(self):
        # auth = AWS4Auth(self.access_key, self.secret_key,
        #                 self.region, 'es', session_token=self.session_token)

        auth = (self.username, self.password)
        self.es = Elasticsearch(
            hosts=[{'host': self.url, 'port': 9243}],
            http_auth=auth,
            scheme="https",
            ssl=True,
        )

    def upload_articles(self, _user_id, _document_type, _title, _year, _author, _text, _es):
        document = {
            "title": _title,
            "year": _year,
            "author" : _author,
            "body": _text
                }
        self.es.index(index=_user_id, doc_type=_document_type, body=document)

    def upload_academic(self, _text, title, year, author, url, index='arxiv'):
        """

        :param _text:
        :return:
        """

        document = {
            "title": title,
            "year": year,
            "author": author,
            "body": _text,
            "url": url
        }
        self.es.index(index=index, doc_type="text", body=document)

    # TODO How to save most recent timestamp each upload so we can check periodically

    # TODO Can we run this script in parallel? Such as downloading all tars, then calling

    # TODO Understand tar errors that are happening frequently


if __name__ == '__main__':
    """
        Downloads tar files one by one.
        Extracts and uploads
        Then Deletes the file
        :return:
    """


    # Create S3 resource & set configs
    setup()

    # Download manifest file to current directory
    download_file('src/arXiv_src_manifest.xml')

    # Explore bucket metadata
    explore_metadata()

    # Begin tar download & extraction
    begin_download()



# es = UploadElasticSearch()
# es.get_config()
# es.es.indices.delete(index='arxiv', ignore=[400, 404])
#

# es = UploadElasticSearch()
# es.get_config()
# # # results = es.es.search(index="arxiv", data="blah 3")
# # # print(results)
# # # print(results[0].body)
# es.upload_academic("am I a new docu", "blah 3", "2015", "derp who", "what what")
#
# # s = Search(using=es.es, index="arxiv") \
# #     .query("match", author="author")   \
#
# s = Search().using(es.es).query("match", body="am i a new")
# response = s.execute()
#
# for hit in s:
#     print(hit.body)

# print(response.__len__())
# for hit in response:
#     print(hit)

# helpers.bulk(es, es.gendata())

