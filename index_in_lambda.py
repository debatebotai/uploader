# -*- coding: utf-8 -*-
"""

"""

import boto3

s3_client = boto3.client('s3')

def handler(event, context):
    bucket = record['s3']['bucket']['name']
    key = record['s3']['object']['key']
    document = str(key)
    s3_client.download_file(bucket, key, document)
    process_file(document)
    s3_client.delete_object(Bucket=bucket, Key=key)


def process_file(_document):
    f = open(_document, 'rb')
    text = f.read()
    return text